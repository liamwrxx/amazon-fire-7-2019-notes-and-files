#install needed programs
sudo apt install python3 python3-serial android-tools-adb android-tools-fastboot

#root device
adb push files/arm/mtk-su /data/local/tmp/
adb shell
cd /data/local/tmp
chmod 755 mtk-su
./mtk-su

##if errors##
#https://forum.xda-developers.com/android/development/amazing-temp-root-mediatek-armv8-t3922213
#It should only take a second or two. If the program gets stuck for more than a few seconds and your device is awake, press Ctrl+C to close it.
#The -v option turns on verbose printing, which is necessary for me to debug any problems.
#########

#https://forum.xda-developers.com/amazon-fire/orig-development/fire-7-2019-mustang-unbrick-downgrade-t3944365
getenforce # Just to confirm it says Permissive
echo 0 > /sys/block/mmcblk0boot0/force_ro
dd if=/dev/zero of=/dev/block/mmcblk0boot0 bs=512 count=8

###start installing custom recovery###
cd files/amonet-mustang/
sudo ./bootrom-step.sh

#you should already have the USB cable plugged in. Type "reboot" in the first terminal (the one you that's running "adb shell"). [If you're trying this for the second time because it didn't work for the first time, you won't have an "adb shell" terminal. In that case, just plugging the USB cable in should be enough.]
#The script will now proceed to downgrade your device and flash some essential files. Just let it be, it will take about 4 minutes. You should see the following output
#If the script freezes at some point, you will have to restart it. Terminate the script, then immediately run `sudo ./bootrom-step.sh` again. The exploit it set up so that after about 40 seconds of inactivity it would reboot your device and drop you back into the bootrom mode, which the script is waiting for. If you cannot restart the process, you might have to open up the tablet and replug the battery to completely power off the device.
#You should see a success message: "Reboot to unlocked fastboot". Only proceed if you see the message.
#Once the device boots to fastboot (check with "fastboot devices"; you should also see amazon logo on the screen.), you can run:


sudo ./fastboot-step.sh


#At this point the device should boot into recovery, however the screen will be off. Just press the power button twice and the screen should turn on.
#Success! You now have a custom recovery installed that can be accessed by holding down power and volume down (the leftmost) buttons. At this point if you came here from a custom ROM thread you should probably follow the ROM installation instructions. However, at the time of writing there are no custom ROMs yet, and the next steps will detail installing a stock firmware and rooting it with Magisk.

##We'll now upload required files to the recovery. On your PC, do:
cd ../
adb push update-kindle-NS6312_user_1827_0002517050244.bin /sdcard/fw.zip
adb push Magisk-v19.3.zip /sdcard
adb push finalize.zip /sdcard

# In the recovery, go to "Install", navigate to "/sdcard" and flash fw.zip
# Go to "Wipe" and do the default wipe, then reboot
# At the Fire setup screen, select your language. On the next screen, Wifi setup, select any password-protected network, then instead of entering the password press "cancel". Now, back at the wifi setup screen, press "Skip setup" and "Skip" in the dialog pop-up again
# Wait for the update to finish (wait until the updating fire notification disappears)
# Hold down the power button, press Restart and hold volume down to boot into recovery.
# In the recovery, go to "Install", navigate to "/sdcard" and flash Magisk-v19.3.zip
# Press back, select finalize.zip and flash it
# Once finalize.zip is flashed, press "Reboot System"

###trouble shooting###
https://forum.xda-developers.com/amazon-fire/orig-development/fire-7-2019-mustang-unbrick-downgrade-t3944365
